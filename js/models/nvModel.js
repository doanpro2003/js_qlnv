function NhanVien(
    _taiKhoan,
    _hoTen,
    _email,
    _matKhau,
    _ngayLam,
    _luongCoBan,
    _chucVu,
    _gioLam
) {
    this.taiKhoan = _taiKhoan;
    this.hoTen = _hoTen;
    this.email = _email;
    this.matKhau = _matKhau;
    this.ngayLam = _ngayLam;
    this.luongCoBan = _luongCoBan;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;

    this.xepLoai = function () {
        var loaiNv = "";
        if (this.gioLam >= 192) loaiNv = "Xuất sắc";
        else if (this.gioLam >= 176) loaiNv = "Giỏi";
        else if (this.gioLam >= 160) loaiNv = "Khá";
        else loaiNv = "Trung Bình";
        return loaiNv;
    };
    this.tinhLuong = function () {
        var luong;
        if (this.chucVu == "Sếp") {
            luong = this.luongCoBan * 3;
        } else if (this.chucVu == "Trưởng phòng") {
            luong = this.luongCoBan * 2;
        } else if (this.chucVu == "Nhân viên") {
            luong = this.luongCoBan * 1;
        }
        return luong;
    };
}
