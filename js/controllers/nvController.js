function layThongTinTuForm() {
    var taiKhoan = document.getElementById("tknv").value;
    var hoTen = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var matKhau = document.getElementById("password").value;
    var ngayLam = document.getElementById("datepicker").value;
    var luongCoBan = document.getElementById("luongCB").value;
    var chucVu = document.getElementById("chucvu").value;
    var gioLam = document.getElementById("gioLam").value;
    return new NhanVien(
        taiKhoan,
        hoTen,
        email,
        matKhau,
        ngayLam,
        luongCoBan,
        chucVu,
        gioLam
    );
}
function renderDsnv(list) {
    var contentHTML = "";
    for (var index = 0; index < list.length; index++) {
        var currentNv = list[index];
        contentHTML += `
        <tr>
            <td>${currentNv.taiKhoan}</td>
            <td>${currentNv.hoTen}</td>
            <td>${currentNv.email}</td>
            <td>${currentNv.ngayLam}</td>
            <td>${currentNv.chucVu}</td>
            <td>${currentNv.tinhLuong()}</td>
            <td>${currentNv.xepLoai()}</td>
            <td>
                <button onclick="capNhat('${
                    currentNv.taiKhoan
                }')" data-toggle="modal" data-target="#myModal" class="btn btn-primary">Cập Nhật</button>
                <button onclick="xoaNv('${
                    currentNv.taiKhoan
                }')" class="btn  btn-danger">Xóa</button>
            </td>
        </tr>
        `;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nv) {
    document.getElementById("tknv").value = nv.taiKhoan;
    document.getElementById("name").value = nv.hoTen;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.matKhau;
    document.getElementById("datepicker").value = nv.ngayLam;
    document.getElementById("luongCB").value = nv.luongCoBan;
    document.getElementById("chucvu").value = nv.chucVu;
    document.getElementById("gioLam").value = nv.gioLam;
}
